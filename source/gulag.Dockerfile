FROM ubuntu:20.04
RUN export DEBIAN_FRONTEND=noninteractive; apt update && apt install -y python3.9 python3-pip build-essential
ADD gulag /gulag
WORKDIR /gulag
RUN python3.9 -m pip install cryptography -r ext/requirements.txt
CMD touch /var/run/nginx.pid; echo -e '\n' | ./main.py
