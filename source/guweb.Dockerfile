FROM python:3.9
ADD guweb /guweb
WORKDIR /guweb
RUN pip install cryptography -r ext/requirements.txt
CMD ["hypercorn", "-b", "unix:/run/guweb.sock", "-m", "0000", "main.py"]