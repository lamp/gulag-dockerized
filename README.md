# gulag-dockerized

World's sexiest gulag setup using docker-compose, IPv6 and Cloudflare. Host- and distro- agnostic... Docker enables easy deployment in any environment that can run Docker, IPv6 lets you have an isolated web server even if you already have a web server on your single IPv4, and Cloudflare provides IPv4 and TLSv1.0 compatibility for you.


## Prerequisites

- Linux host with publically routable block of IPv6 addresses and latest versions of [Docker](https://docs.docker.com/engine/install/) and [docker-compose](https://docs.docker.com/compose/install/) installed.

- A domain name added to Cloudflare. You can get a domain for free at http://dot.tk.


## 1. Enable IPv6 in Docker

- See https://docs.docker.com/config/daemon/ipv6/

- Find your host's global IPv6 + prefix length (i.e. `2605:a140:2045:1635::1/64`) under your network interface (i.e. `eth0`) in `ip a`.

- Decide on a subnet to provide to Docker. You can use the reference below. For example, I'm using  `2605:a140:2045:1635:69::/80`. Put this in your daemon.json and restart docker.

<details>
<summary>IPv6 Subnet Reference Prefix Lengths:</summary>
<pre>
2402:9400:0000:0000:0000:0000:0000:0001
XXXX:XXXX:XXXX:XXXX:XXXX:XXXX:XXXX:XXXX
      ||| |||| |||| |||| |||| |||| ||||
      ||| |||| |||| |||| |||| |||| |||128
      ||| |||| |||| |||| |||| |||| ||124
      ||| |||| |||| |||| |||| |||| |120
      ||| |||| |||| |||| |||| |||| 116
      ||| |||| |||| |||| |||| |||112
      ||| |||| |||| |||| |||| ||108
      ||| |||| |||| |||| |||| |104
      ||| |||| |||| |||| |||| 100
      ||| |||| |||| |||| |||96
      ||| |||| |||| |||| ||92
      ||| |||| |||| |||| |88
      ||| |||| |||| |||| 84
      ||| |||| |||| |||80
      ||| |||| |||| ||76
      ||| |||| |||| |72
      ||| |||| |||| 68
      ||| |||| |||64
      ||| |||| ||60
      ||| |||| |56
      ||| |||| 52
      ||| |||48
      ||| ||44
      ||| |40
      ||| 36
      ||32
      |28
      24
</pre>
</details>


## 2. Set up gulag-dockerized

- `cd /opt` or wherever you want

- `git clone https://gitea.moe/lamp/gulag-dockerized.git --recursive && cd gulag-dockerized`

- `cp -r config-template config`

- Edit all files in `config` folder as necessary. Replace all instances of example.com with your domain, add your API keys, etc.

- Run `docker-compose up -d mysql && docker-compose logs -f &` to initialize the database and start following logs in background.

- Once that's finished, run `docker-compose up -d` to bring up the rest of the containers. You can type `fg` and then press ctrl C to stop the log output.

- Find nginx IPv6 address: `docker inspect gulag-dockerized_nginx_1 | grep GlobalIPv6Address`. Example: `2605:a140:2045:1635:69:242:ac11:8`. Add this to your Cloudflare DNS...


## 3. Set up Cloudflare
- Configure your DNS like so:

![](https://eqf4.qonq.gq/)

- On the SSL/TLS tab, make sure mode is set to **Flexible**.


## 4. Use in OSU

- Find OSU and open file location.

![](https://wx9e.qonq.gq)

- Copy the shortcut to your Desktop.

- Edit the shortcut, and in the Target field, add ` -devserver <yourdomain>`.

![](https://z0lv.qonq.gq)

- Use this shortcut to launch OSU with your gulag server!
